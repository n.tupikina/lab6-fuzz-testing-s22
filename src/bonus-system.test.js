import { calculateBonuses }  from "./bonus-system.js";
const assert = require("assert");
//const closeTo = require("");


describe('Bonus System tests', () => {
    let app;
    console.log("Tests started");


    test('Standard < 10 000',  (done) => {
        let a = "Standard";
        let b = 5000;
        expect(calculateBonuses(a,b)).toEqual(0.05*1);
        done();
    });

    test('Premium < 10 000',  (done) => {
        let a = "Premium";
        let b = 5000;
        assert.equal(calculateBonuses(a,b),0.1*1);
        done();
    });

    test('Diamond < 10 000',  (done) => {
        let a = "Diamond";
        let b = 5000;
        assert.equal(calculateBonuses(a,b),0.2*1);
        done();
    });

    test('Nonsense < 10 000', (done) => {
        let a = "Nonsense";
        let b = 5000;
        assert.equal(calculateBonuses(a,b),0);
        done();
    });
	
	test('Standard =10 000', (done) => {
        let a = "Standard";
        let b = 10000;
        assert.equal(calculateBonuses(a,b),0.05*1.5);
        done();
    });

    test('Premium 10 000', (done) => {
        let a = "Premium";
        let b = 10000;
        assert.equal(calculateBonuses(a,b),0.1*1.5);
        done();
    });


    test('Diamond =10 000',  (done) => {
        let a ="Diamond";
        let b = 10000;
        assert.equal(calculateBonuses(a,b), 0.2*1.5);
        done();
    });

    test('Nonsense =10 000',  (done) => {
        let a = "Nonsense";
        let b = 10000;
        assert.equal(calculateBonuses(a,b),0);
        done();
    });

    test('Standard <50 000', (done) => {
        let a = "Standard";
        let b = 15000;
        assert.equal(calculateBonuses(a,b),0.05*1.5);
        done();
    });

    test('Premium <50 000', (done) => {
        let a = "Premium";
        let b = 15000;
        assert.equal(calculateBonuses(a,b),0.1*1.5);
        done();
    });


    test('Diamond <50 000',  (done) => {
        let a ="Diamond";
        let b = 15000;
        assert.equal(calculateBonuses(a,b), 0.2*1.5);
        done();
    });

    test('Nonsense <50 000',  (done) => {
        let a = "Nonsense";
        let b = 15000;
        assert.equal(calculateBonuses(a,b),0);
        done();
    });
	
	test('Standard =50 000',  (done) => {
        let a = "Standard";
        let b = 50000;
        assert.equal(calculateBonuses(a,b),0.05*2);
        done();
    });

    test('Premium =50 000',  (done) => {
        let a = "Premium";
        let b = 50000;
        assert.equal(calculateBonuses(a,b),0.1*2)
        done();
    });

    test('Diamond =50 000',  (done) => {
        let a = "Diamond";
        let b = 50000;
        assert.equal(calculateBonuses(a,b),0.2*2)
        done();
    });

    test('Nonsense =50 000',  (done) => {
        let a = "Nonsense";
        let b = 50000;
        assert.equal(calculateBonuses(a,b),0)
        done();
    });

    test('Standard <100 000',  (done) => {
        let a = "Standard";
        let b = 75000;
        assert.equal(calculateBonuses(a,b),0.05*2);
        done();
    });

    test('Premium <100 000',  (done) => {
        let a = "Premium";
        let b = 75000;
        assert.equal(calculateBonuses(a,b),0.1*2)
        done();
    });

    test('Diamond <100 000',  (done) => {
        let a = "Diamond";
        let b = 75000;
        assert.equal(calculateBonuses(a,b),0.2*2)
        done();
    });

    test('Nonsense <100 000',  (done) => {
        let a = "Nonsense";
        let b = 75000;
        assert.equal(calculateBonuses(a,b),0)
        done();
    });
	
	test('Standard =100 000',  (done) => {
        let a = "Standard";
        let b = 100000;
        assert.equal(calculateBonuses(a,b),0.05*2.5);
        done();
    });

    test('Premium =100 000',  (done) => {
        let a = "Premium";
        let b = 100000;
        assert.equal(calculateBonuses(a,b),0.1*2.5);
        done();
    });
	
	test('Diamond =100 000',  (done) => {
        let a = "Diamond";
        let b = 100000;
        assert.equal(calculateBonuses(a,b),0.2*2.5);
        done();
    });
	
	test('Nonsense =100 000',  (done) => {
        let a = "Nonsense";
        let b = 100000;
        assert.equal(calculateBonuses(a,b),0);
        done();
    });


    test('Standard >100 000',  (done) => {
        let a = "Standard";
        let b = 125000;
        assert.equal(calculateBonuses(a,b),0.05*2.5);
        done();
    });

    test('Premium >100 000',  (done) => {
        let a = "Premium";
        let b = 125000;
        assert.equal(calculateBonuses(a,b),0.1*2.5);
        done();
    });
	
	test('Diamond >100 000',  (done) => {
        let a = "Diamond";
        let b = 125000;
        assert.equal(calculateBonuses(a,b),0.2*2.5);
        done();
    });
	
	test('Nonsense >100 000',  (done) => {
        let a = "Nonsense";
        let b = 125000;
        assert.equal(calculateBonuses(a,b),0);
        done();
    });


    console.log('Tests Finished');

});
